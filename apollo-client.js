// ./apollo-client.js

import { ApolloClient, InMemoryCache } from "@apollo/client";
console.log(import.meta);
const client = new ApolloClient({
  uri: `${import.meta.env.VITE_API_URL || ""}/graphql`,
  cache: new InMemoryCache({ addTypename: false, connectToDevTools: true }),
});

export default client;
