import React, { useEffect } from "react";
import { createRef } from "react";
import ReactDOM from "react-dom/client";
import "./index.scss";
import "./styles/globals.scss";
import { ApolloProvider } from "@apollo/client";
import {
  createBrowserRouter,
  RouterProvider,
  useOutlet,
} from "react-router-dom";
import client from "../apollo-client";
import Main from "./pages/Main";
import Contacts from "./pages/Contacts";
import Case from "./pages/Case";
import About from "./pages/About";
import NotFound from "./pages/NotFound";
import Portfolio from "./pages/Portfolio";
import Terms from "./pages/Terms";
import { CSSTransition, SwitchTransition } from "react-transition-group";
import { useLocation } from "react-router-dom";
import ReactGA from "react-ga4";
import Privacy from "./pages/Privacy";
import CookiePolicy from "./pages/CookiePolicy";

const routes = [
  { path: "/", element: <Main />, nodeRef: createRef() },
  { path: "/works", element: <Portfolio />, nodeRef: createRef() },
  { path: "/about", element: <About />, nodeRef: createRef() },
  { path: "/case/:id", element: <Case />, nodeRef: createRef() },
  { path: "/contacts", element: <Contacts />, nodeRef: createRef() },
  { path: "/terms-of-use", element: <Terms />, nodeRef: createRef() },
  { path: "/privacy-policy", element: <Privacy />, nodeRef: createRef() },
  { path: "/cookie-policy", element: <CookiePolicy />, nodeRef: createRef() },
  { path: "*", element: <NotFound />, nodeRef: createRef() },
];

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    children: routes.map((route) => ({
      index: route.path === "/",
      path: route.path === "/" ? undefined : route.path,
      element: route.element,
    })),
  },
]);

function App() {
  const location = useLocation();
  const currentOutlet = useOutlet();
  const { nodeRef } =
    routes.find((route) => route.path === location.pathname) ?? {};

  ReactGA.initialize("G-X24FJ4V41B");

  useEffect(() => {
    ReactGA.send({ hitType: "pageview", page: location.pathname });
  }, [location]);

  window.oncontextmenu = function () {
    return false;
  };
  document.onkeydown = function (e) {
    if (window.event.keyCode == 123 || e.button == 2) return false;
  };

  return (
    <SwitchTransition>
      <CSSTransition
        key={location.key}
        classNames="fade"
        timeout={300}
        nodeRef={nodeRef}
        unmountOnExit
      >
        {(state) => (
          <div ref={nodeRef} className="page">
            {currentOutlet}
          </div>
        )}
      </CSSTransition>
    </SwitchTransition>
  );
}

ReactDOM.createRoot(document.getElementById("root")).render(
  <ApolloProvider client={client}>
    <RouterProvider router={router} />
  </ApolloProvider>
);
