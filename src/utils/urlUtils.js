export const setAbsolutePath = (link) => {
  return `${import.meta.env.VITE_API_URL}${link}`;
};
