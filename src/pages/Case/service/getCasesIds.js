import { gql } from "@apollo/client";

export const GetCasesIds = gql`
  query GetCasesIds {
    cases(pagination: { start: 0, limit: 1000 }) {
      data {
        id
      }
    }
  }
`;
