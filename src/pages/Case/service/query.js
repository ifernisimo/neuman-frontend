import { gql } from "@apollo/client";

export const GetCase = gql`
  query getCase($id: ID!) {
    case(id: $id) {
      data {
        id
        attributes {
          title
          description
          cover {
            data {
              attributes {
                url
              }
            }
          }
          section {
            sub_description
            images {
              data {
                attributes {
                  url
                }
              }
            }
            position
          }
        }
      }
    }
  }
`;
