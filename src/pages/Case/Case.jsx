import Layout from "../../components/Layout";
import { GetCase } from "./service/query";
import { useQuery } from "@apollo/client";
import { useNavigate, useParams } from "react-router-dom";
import { setAbsolutePath } from "../../utils/urlUtils";
import styles from "./Case.module.scss";
import { Link } from "react-router-dom";
import arrowLeft from "../../assets/images/arrow-left.svg";
import arrowRight from "../../assets/images/arrow-right.svg";
import ReactGA from "react-ga4";
import { Helmet } from "react-helmet";
import { GetCasesIds } from "./service/getCasesIds";
import { useEffect, useState } from "react";
import classNames from "classnames";
import lodash_get from "lodash/get";

export const Case = () => {
  let { id } = useParams();
  const navigate = useNavigate();
  const [clearIds, setClearIds] = useState([]);
  const [nextCase, setNextCase] = useState(1);
  const [prevCase, setPrevCase] = useState(1);
  const [nextBtnHidden, setNextBtnHidden] = useState(false);
  const [nextPrevHidden, setPrevBtnHidden] = useState(false);

  const { loading, error, data } = useQuery(GetCase, {
    variables: {
      id,
    },
  });

  const {
    loading: loadingIds,
    error: errorIds,
    data: dataIds,
  } = useQuery(GetCasesIds);

  useEffect(() => {
    if (dataIds && !loading) {
      const clear = dataIds.cases.data.map((item) => item.id);
      setClearIds(clear);
    }
  }, [loading]);

  if (!loading && !lodash_get(data, "case.data")) {
    navigate("/works");
  }

  useEffect(() => {
    if (clearIds.length) {
      setNextId();
      setPrevId();
    }
  });

  const createMarkup = (text) => {
    return { __html: text };
  };

  const GaLogEvent = ({ label }) => {
    ReactGA.event({
      category: "Case view",
      action: "case_pagination",
      label,
      nonInteraction: true,
    });
  };

  const setNextId = () => {
    const currentIndex = clearIds.indexOf(id);
    const nextIfExist = clearIds.indexOf(clearIds[currentIndex + 1]) !== -1;

    setNextBtnHidden(nextIfExist);
    if (nextIfExist) {
      setNextCase(clearIds[currentIndex + 1]);
      GaLogEvent(nextCase);
    }
  };

  const setPrevId = () => {
    const currentIndex = clearIds.indexOf(id);
    const backIfExist = clearIds.indexOf(clearIds[currentIndex - 1]) !== -1;

    setPrevBtnHidden(backIfExist);
    if (backIfExist) {
      setPrevCase(clearIds[currentIndex - 1]);
      GaLogEvent(prevCase);
    }
  };

  return (
    <>
      <Helmet>
        <title>
          Irene Neyman / {lodash_get(data, "case.data.attributes.title", "")}
        </title>
      </Helmet>
      {!loading ? (
        <Layout>
          <div className={styles.case}>
            <h1 className={styles.case_title}>
              {lodash_get(data, "case.data.attributes.title", "")}
            </h1>
            <div
              className={styles.case_description}
              dangerouslySetInnerHTML={createMarkup(
                lodash_get(data, "case.data.attributes.description", "")
              )}
            ></div>
            {lodash_get(data, "case.data.attributes.section", []).map(
              (block, index) => (
                <div key={index} className={styles.section_wrapper}>
                  {!block.position ? (
                    <p
                      className={styles.images_description}
                      style={{ paddingBottom: "32px" }}
                      dangerouslySetInnerHTML={createMarkup(
                        block.sub_description
                      )}
                    ></p>
                  ) : (
                    <></>
                  )}

                  <div className={styles.images_row}>
                    {block.images.data.map((image) => {
                      const width = 100 / block.images.data.length + "%";

                      return (
                        <div
                          key={image.attributes.url}
                          className={styles.image_wrapper}
                          style={{ width: width }}
                        >
                          <img
                            className={styles.image}
                            src={setAbsolutePath(image.attributes.url)}
                            alt=""
                          />
                        </div>
                      );
                    })}
                  </div>
                  {block.position ? (
                    <p
                      className={styles.images_description}
                      dangerouslySetInnerHTML={createMarkup(
                        block.sub_description
                      )}
                    ></p>
                  ) : (
                    <></>
                  )}
                </div>
              )
            )}

            <div className={styles.pagination}>
              {nextBtnHidden ? (
                <Link
                  className={styles.arrowLeft}
                  to={`/case/${nextCase}`}
                  alt="Previous case"
                >
                  <div className={styles.arrow}>
                    <img src={arrowLeft} alt="" />
                    <span>Previous Case</span>
                  </div>
                </Link>
              ) : (
                <></>
              )}

              {nextPrevHidden ? (
                <Link
                  className={styles.arrowRight}
                  to={`/case/${prevCase}`}
                  alt="Next case"
                >
                  <div className={styles.arrow}>
                    <span>Next Case</span>
                    <img src={arrowRight} alt="" />
                  </div>
                </Link>
              ) : (
                <></>
              )}
            </div>
          </div>
        </Layout>
      ) : (
        <></>
      )}
    </>
  );
};
