import Layout from "../../components/Layout";
import styles from "./Contacts.module.scss";
import star from "../../assets/images/star.svg";
import axios from "axios";
import { Helmet } from "react-helmet";
import ReCAPTCHA from "react-google-recaptcha-enterprise";

export const Contacts = () => {
  const handleSubmit = (e) => {
    e.preventDefault();

    const data = { Field: "dcsdc" };

    axios
      .post(
        "https://sheet.best/api/sheets/6ba4514f-ca3f-49bb-9b7d-187ae8d480c5",
        data
      )
      .then((res) => console.log(res));
  };

  const onHandleChange = (data) => {
    console.log(data);
  };

  return (
    <>
      <Helmet>
        <title>Irene Neyman / Contacts</title>
      </Helmet>
      <Layout>
        <div className={styles.contacts}>
          <h1>
            <span>Ready to create something</span>
            <span>magical together?</span>
          </h1>

          <p>
            If you have any work inquiries, collaboration ideas, questions, or
            just want to say hi, please reach out to me via e-mail or through
            the contact form below.
          </p>

          <a
            className={styles.email}
            href={
              "mailto:ireneneyman.illustrator@gmail.com?subject=Website_email"
            }
          >
            ireneneyman.illustrator@gmail.com
          </a>

          <iframe
            src="https://docs.google.com/forms/d/e/1FAIpQLSfgQqRJt3DWOEdSog7BdQAJ-MmQOR_IHckmr-nENDsEHabstQ/viewform?embedded=true"
            width="640"
            height="718"
            frameborder="0"
            marginheight="0"
            marginwidth="0"
          >
            Загрузка…
          </iframe>
          {/* <form onSubmit={(e) => handleSubmit(e)}>
            <ReCAPTCHA
              sitekey="6LdFetskAAAAAPl4Jd64LP2O22sefMsUYzmSRoOR"
              onChange={onHandleChange}
            />
            <button type="submit">Submit</button>
          </form> */}
        </div>
      </Layout>
    </>
  );
};
