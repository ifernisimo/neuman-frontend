import Layout from "../../components/Layout";
import styles from "./Privacy.module.scss";
import classnames from "classnames";
import { Helmet } from "react-helmet";
import * as Scroll from "react-scroll";

export const Privacy = () => {
  const anchors = [
    { id: "information-we-collect", name: "WHAT INFORMATION DO WE COLLECT?" },
    { id: "how-we-process", name: "HOW DO WE PROCESS YOUR INFORMATION?" },
    {
      id: "legal-bases-rely",
      name: "WHAT LEGAL BASES DO WE RELY ON TO PROCESS YOUR INFORMATION?",
    },
    {
      id: "with-whom-share",
      name: "WHEN AND WITH WHOM DO WE SHARE YOUR PERSONAL INFORMATION?",
    },
    {
      id: "do-we-use-cookie",
      name: "DO WE USE COOKIES AND OTHER TRACKING TECHNOLOGIES?",
    },
    {
      id: "information-transferred",
      name: "IS YOUR INFORMATION TRANSFERRED INTERNATIONALLY?",
    },
    { id: "how-long-we-keep", name: "HOW LONG DO WE KEEP YOUR INFORMATION?" },
    {
      id: "your-information-safe",
      name: "HOW DO WE KEEP YOUR INFORMATION SAFE?",
    },
    {
      id: "collect-from-minors",
      name: "DO WE COLLECT INFORMATION FROM MINORS?",
    },
    { id: "privacy-rights", name: "WHAT ARE YOUR PRIVACY RIGHTS?" },
    { id: "do-not-track-features", name: "CONTROLS FOR DO-NOT-TRACK FEATURES" },
    {
      id: "california-residents",
      name: "DO CALIFORNIA RESIDENTS HAVE SPECIFIC PRIVACY RIGHTS?",
    },
    { id: "update-to-notice", name: "DO WE MAKE UPDATES TO THIS NOTICE?" },
    { id: "how-contact-us", name: "HOW CAN YOU CONTACT US ABOUT THIS NOTICE?" },
    {
      id: "review-or-delete",
      name: "HOW CAN YOU REVIEW, UPDATE, OR DELETE THE DATA WE COLLECT FROM YOU?",
    },
  ];

  return (
    <>
      <Helmet>
        <title>Irene Neyman / Privacy policy</title>
      </Helmet>
      <Layout>
        <div className={styles.terms}>
          <h1>Privacy policy</h1>
          <div className={classnames(styles.about, styles.section)}>
            <span className={styles.lastUpdate}>
              Last updated January 18, 2023
            </span>
          </div>
          <div className={classnames(styles.disclaimer, styles.section)}>
            <p>
              This privacy policy explains how Iryna Yancheva, doing business as
              Irene Neyman ("<b>Irene Neyman,</b>" "<b>we,</b>" "<b>us,</b>" or
              "<b>our</b>"), collects, stores, uses, and/or share ('process')
              your information when you use our services ('<b>Services</b>'),
              such as when you:
            </p>
            <ul>
              <li>
                Visit our website at
                <a className="link" href="https://ireneneyman.com">
                  &nbsp;https://ireneneyman.com
                </a>
                , or any website of ours that links to this privacy policy
              </li>
              <li>
                Engage with us in other related ways, including any sales,
                marketing, or events
              </li>
            </ul>
            <p>
              <b>Questions or concerns?</b> Reading this privacy policy will
              help you understand your privacy rights and choices. If you do not
              agree with our policies and practices, please do not use our
              Services. If you still have any questions or concerns, please
              contact us at
              <a
                className="link"
                href="mailto:ireneneyman.illustrator@gmail.com"
              >
                &nbsp;ireneneyman.illustrator@gmail.com.
              </a>
            </p>
          </div>
          <div className={classnames(styles.keyPoints, styles.section)}>
            <h3>SUMMARY OF KEY POINTS</h3>
            <p>
              <b>
                This summary provides key points from our privacy policy, but
                you can find out more details about any of these topics by
                clicking the link following each key point or by using our table
                of contents below to find the section you are looking for. You
                can also click
                <Scroll.Link
                  activeClass="active"
                  to={"table-of-content"}
                  spy={true}
                  smooth={true}
                  offset={-100}
                  duration={1000}
                >
                  &nbsp;<a className="link">here</a>&nbsp;
                </Scroll.Link>
                to go directly to our table of contents.
              </b>
            </p>
            <p>
              <b>What personal information do we process?</b> When you visit,
              use, or navigate our Services, we may process personal information
              depending on how you interact with Irene Neyman and the Services,
              the choices you make, and the products and features you use. Click
              <Scroll.Link
                activeClass="active"
                to={"information-we-collect"}
                spy={true}
                smooth={true}
                offset={-100}
                duration={1000}
              >
                &nbsp;<a className="link">here</a>&nbsp;
              </Scroll.Link>
              to learn more.
            </p>
            <p>
              <b>Do we process any sensitive personal information?</b> We do not
              process sensitive personal information.
            </p>
            <p>
              <b>Do we receive any information from third parties?</b> We do not
              receive any information from third parties.
            </p>
            <p>
              <b>How do we process your information?</b> We process your
              information to provide, improve, and administer our Services,
              communicate with you, for security and fraud prevention, and to
              comply with the law. We may also process your information for
              other purposes with your consent. We process your information only
              when we have a valid legal reason to do so. Click
              <Scroll.Link
                activeClass="active"
                to={"how-we-process"}
                spy={true}
                smooth={true}
                offset={-100}
                duration={1000}
              >
                &nbsp;<a className="link">here</a>&nbsp;
              </Scroll.Link>
              to learn more.
            </p>
            <p>
              <b>
                In what situations and with which types of parties do we share
                personal information?
              </b>
              We may share information in specific situations and with specific
              categories of third parties. Click here to learn more.
            </p>
            <p>
              <b>How do we keep your information safe?</b> We have
              organizational and technical processes and procedures in place to
              protect your personal information. However, no electronic
              transmission over the internet or information storage technology
              can be guaranteed to be 100% secure, so we cannot promise or
              guarantee that hackers, cybercriminals, or other unauthorized
              third parties will not be able to defeat our security and
              improperly collect, access, steal, or modify your information.
              Click here to learn more.
            </p>
            <p>
              <b>What are your rights?</b> Depending on where you are located
              geographically, the applicable privacy law may mean you have
              certain rights regarding your personal information. Click here to
              learn more.
            </p>
            <p>
              <b>How do you exercise your rights?</b> The easiest way to
              exercise your rights is by contacting us. We will consider and act
              upon any request in accordance with applicable data protection
              laws.
            </p>
            <p>
              Want to learn more about what Irene Neyman does with any
              information we collect? Click here to review the notice in full.
            </p>
          </div>
          <div
            id="table-of-content"
            className={classnames(styles.contents, styles.section)}
          >
            <h3>TABLE OF CONTENTS</h3>

            <div>
              <ol>
                {anchors.map((item) => {
                  return (
                    <li key={item.id}>
                      <Scroll.Link
                        activeClass="active"
                        to={item.id}
                        spy={true}
                        smooth={true}
                        offset={-100}
                        duration={1000}
                      >
                        <a>{item.name}</a>
                      </Scroll.Link>
                    </li>
                  );
                })}
              </ol>
            </div>
          </div>
          <Scroll.Element id="information-we-collect">
            <div className={classnames(styles.infoWeCollect, styles.section)}>
              <h3>1. WHAT INFORMATION DO WE COLLECT?</h3>

              <p>
                <b>Personal information you disclose to us</b>
              </p>
              <p>
                <b>In Short:</b> We collect personal information that you
                provide to us.
              </p>
              <p>
                We collect personal information that you voluntarily provide to
                us when you express an interest in obtaining information about
                us or our products and Services, when you participate in
                activities on the Services, or otherwise when you contact us.
              </p>
              <p>
                Personal Information Provided by You. The personal information
                that we collect depends on the context of your interactions with
                us and the Services, the choices you make, and the products and
                features you use. The personal information we collect may
                include the following:
              </p>
              <ul>
                <li>names</li>
                <li>email addresses</li>
              </ul>
              <p>
                <b>Sensitive Information.</b> We do not process sensitive
                information.
              </p>
              <p>
                All personal information that you provide to us must be true,
                complete, and accurate, and you must notify us of any changes to
                such personal information.
              </p>
              <p>
                <b>Information automatically collected</b>
              </p>
              <p>
                <b>In Short:</b> Some information — such as your Internet
                Protocol (IP) address and/or browser and device characteristics
                — is collected automatically when you visit our Services.
              </p>
              <p>
                We automatically collect certain information when you visit,
                use, or navigate the Services. This information does not reveal
                your specific identity (like your name or contact information)
                but may include device and usage information, such as your IP
                address, browser and device characteristics, operating system,
                language preferences, referring URLs, device name, country,
                location, information about how and when you use our Services,
                and other technical information. This information is primarily
                needed to maintain the security and operation of our Services,
                and for our internal analytics and reporting purposes.
              </p>
              <p>
                Like many businesses, we also collect information through
                cookies and similar technologies.
              </p>
              <p>The information we collect includes:</p>
              <ul>
                <li>
                  Log and Usage Data. Log and usage data are service-related,
                  diagnostic, usage, and performance information our servers
                  automatically collect when you access or use our Services and
                  which we record in log files. Depending on how you interact
                  with us, this log data may include your IP address, device
                  information, browser type, and settings and information about
                  your activity in the Services (such as the date/time stamps
                  associated with your usage, pages and files viewed, searches,
                  and other actions you take such as which features you use),
                  device event information (such as system activity, error
                  reports (sometimes called 'crash dumps'), and hardware
                  settings).
                </li>
              </ul>
            </div>
          </Scroll.Element>
          <Scroll.Element id="how-we-process">
            <div className={classnames(styles.weProcess, styles.section)}>
              <h3>2. HOW DO WE PROCESS YOUR INFORMATION?</h3>
              <p>
                <b>In Short:</b> We process your information to provide,
                improve, and administer our Services, communicate with you, for
                security and fraud prevention, and to comply with law. We may
                also process your information for other purposes with your
                consent.
              </p>

              <p>
                <b>
                  We process your personal information for a variety of reasons,
                  depending on how you interact with our Services, including:
                </b>
              </p>
              <ul>
                <li>
                  To respond to user inquiries/offer support to users. We may
                  process your information to respond to your inquiries and
                  solve any potential issues you might have with the requested
                  service.
                </li>
              </ul>
            </div>
          </Scroll.Element>
          <Scroll.Element id="legal-bases-rely">
            <div className={classnames(styles.legalBases, styles.section)}>
              <h3>
                3. WHAT LEGAL BASES DO WE RELY ON TO PROCESS YOUR INFORMATION?
              </h3>
              <p>
                <b>In Short:</b> We only process your personal information when
                we believe it is necessary and we have a valid legal reason
                (i.e. legal basis) to do so under applicable law, like with your
                consent, to comply with laws, to provide you with services to
                enter into or fulfil our contractual obligations, to protect
                your rights, or to fulfil our legitimate business interests.
              </p>
              <p>
                <b>
                  If you are located in the EU or UK, this section applies to
                  you.
                </b>
              </p>
              <p>
                The General Data Protection Regulation (GDPR) and UK GDPR
                require us to explain the valid legal bases we rely on in order
                to process your personal information. As such, we may rely on
                the following legal bases to process your personal information:
              </p>
              <ul>
                <li>
                  <b>Consent.</b> We may process your information if you have
                  given us permission (i.e. consent) to use your personal
                  information for a specific purpose. You can withdraw your
                  consent at any time. Click here to learn more.
                </li>
                <li>
                  <b>Performance of a Contract.</b>
                  We may process your personal information when we believe it is
                  necessary to fulfil our contractual obligations to you,
                  including providing our Services or at your request prior to
                  entering into a contract with you.
                </li>
                <li>
                  <b>Legal Obligations.</b> We may process your information
                  where we believe it is necessary for compliance with our legal
                  obligations, such as to cooperate with a law enforcement body
                  or regulatory agency, exercise or defend our legal rights, or
                  disclose your information as evidence in litigation in which
                  we are involved.
                </li>
                <li>
                  <b>Vital Interests.</b> We may process your information where
                  we believe it is necessary to protect your vital interests or
                  the vital interests of a third party, such as in situations
                  involving potential threats to the safety of any person.
                </li>
              </ul>
              <p>
                <b>
                  If you are located in Canada, this section applies to you.
                </b>
              </p>
              <p>
                We may process your information if you have given us specific
                permission (i.e. express consent) to use your personal
                information for a specific purpose, or in situations where your
                permission can be inferred (i.e. implied consent). You can
                withdraw your consent at any time. Click here to learn more.
              </p>
              <p>
                In some exceptional cases, we may be legally permitted under
                applicable law to process your information without your consent,
                including, for example:
              </p>
              <ul>
                <li>
                  If collection is clearly in the interests of an individual and
                  consent cannot be obtained in a timely way
                </li>
                <li>For investigations and fraud detection and prevention</li>
                <li>
                  If it is contained in a witness statement and the collection
                  is necessary to assess, process, or settle an insurance claim
                </li>
                <li>
                  For identifying injured, ill, or deceased persons and
                  communicating with next of kin
                </li>
                <li>
                  If we have reasonable grounds to believe an individual has
                  been, is, or may be the victim of financial abuse
                </li>
                <li>
                  If it is reasonable to expect collection and use with consent
                  would compromise the availability or the accuracy of the
                  information and the collection is reasonable for purposes
                  related to investigating a breach of an agreement or a
                  contravention of the laws of Canada or a province
                </li>
                <li>
                  If disclosure is required to comply with a subpoena, warrant,
                  court order, or rules of the court relating to the production
                  of records
                </li>
                <li>
                  If it was produced by an individual in the course of their
                  employment, business, or profession and the collection is
                  consistent with the purposes for which the information was
                  produced
                </li>
                <li>
                  If the collection is solely for journalistic, artistic, or
                  literary purposes
                </li>
                <li>
                  If the information is publicly available and is specified by
                  the regulations
                </li>
              </ul>
            </div>
          </Scroll.Element>
          <Scroll.Element id="with-whom-share">
            <div className={classnames(styles.with_whom_share, styles.section)}>
              <h3>
                4. WHEN AND WITH WHOM DO WE SHARE YOUR PERSONAL INFORMATION?
              </h3>
              <p>
                <b>In Short:</b> We may share information in specific situations
                described in this section and/or with the following categories
                of third parties.
              </p>
              <p>
                <b>
                  Vendors, Consultants, and Other Third-Party Service Providers.
                </b>
                We may share your data with third-party vendors, service
                providers, contractors, or agents (<b>'third parties'</b>) who
                perform services for us or on our behalf and require access to
                such information to do that work. We have contracts in place
                with our third parties, which are designed to help safeguard
                your personal information. This means that they cannot do
                anything with your personal information unless we have
                instructed them to do it. They will also not share your personal
                information with any organization apart from us. They also
                commit to protect the data they hold on our behalf and to retain
                it for the period we instruct. The categories of third parties
                we may share personal information with are as follows:
              </p>
              <ul>
                <li>Data Analytics Services</li>
              </ul>
            </div>
          </Scroll.Element>
          <Scroll.Element id="do-we-use-cookie">
            <div
              className={classnames(styles.do_we_use_cookie, styles.section)}
            >
              <h3>5. DO WE USE COOKIES AND OTHER TRACKING TECHNOLOGIES?</h3>
              <p>
                <b>In Short:</b> We may use cookies and other tracking
                technologies to collect and store your information.
              </p>
              <p>
                We may use cookies and similar tracking technologies (like web
                beacons and pixels) to access or store information. Specific
                information about how we use such technologies and how you can
                refuse certain cookies is set out in our&nbsp;
                <a className="link" href="/cookie-policy">
                  Cookie Policy
                </a>
                .
              </p>
            </div>
          </Scroll.Element>
          <Scroll.Element id="information-transferred">
            <div
              className={classnames(
                styles.information_transferred,
                styles.section
              )}
            >
              <h3>6. IS YOUR INFORMATION TRANSFERRED INTERNATIONALLY?</h3>
              <p>
                <b>In Short:</b> We may transfer, store, and process your
                information in countries other than your own.
              </p>
              <p>
                Our servers are located in the United States. If you are
                accessing our Services from outside the United States, please be
                aware that your information may be transferred to, stored, and
                processed by us in our facilities and by those third parties
                with whom we may share your personal information (see{" "}
                <Scroll.Link
                  activeClass="active"
                  to={"with-whom-share"}
                  spy={true}
                  smooth={true}
                  offset={-100}
                  duration={1000}
                >
                  &nbsp;
                  <a className="link">
                    'WHEN AND WITH WHOM DO WE SHARE YOUR PERSONAL INFORMATION?'
                  </a>
                  &nbsp;
                </Scroll.Link>
                above), in the United States, and other countries.
              </p>
              <p>
                If you are a resident in the European Economic Area (EEA) or the
                United Kingdom (UK), then these countries may not necessarily
                have data protection laws or other similar laws as comprehensive
                as those in your country. However, we will take all necessary
                measures to protect your personal information in accordance with
                this privacy policy and applicable law.
              </p>
              <p>
                <b>European Commission's Standard Contractual Clauses:</b>
              </p>
              <p>
                We have implemented measures to protect your personal
                information, including by using the European Commission's
                Standard Contractual Clauses for transfers of personal
                information between our group companies and between us and our
                third-party providers. These clauses require all recipients to
                protect all personal information that they process originating
                from the EEA or UK in accordance with European data protection
                laws and regulations. Our Standard Contractual Clauses can be
                provided upon request. We have implemented similar appropriate
                safeguards with our third-party service providers and partners
                and further details can be provided upon request.
              </p>
            </div>
          </Scroll.Element>
          <Scroll.Element id="how-long-we-keep">
            <div
              className={classnames(styles.how_long_we_keep, styles.section)}
            >
              <h3>7. HOW LONG DO WE KEEP YOUR INFORMATION?</h3>
              <p>
                <b>In Short:</b> We keep your information for as long as
                necessary to fulfil the purposes outlined in this privacy policy
                unless otherwise required by law.
              </p>
              <p>
                We have implemented appropriate and reasonable technical and
                organizational security measures designed to protect the
                security of any personal information we process. However,
                despite our safeguards and efforts to secure your information,
                no electronic transmission over the Internet or information
                storage technology can be guaranteed to be 100% secure, so we
                cannot promise or guarantee that hackers, cybercriminals, or
                other unauthorized third parties will not be able to defeat our
                security and improperly collect, access, steal, or modify your
                information. Although we will do our best to protect your
                personal information, the transmission of personal information
                to and from our Services is at your own risk. You should only
                access the Services within a secure environment.
              </p>
            </div>
          </Scroll.Element>
          <Scroll.Element id="your-information-safe">
            <div
              className={classnames(
                styles.your_information_safe,
                styles.section
              )}
            >
              <h3>8. HOW DO WE KEEP YOUR INFORMATION SAFE?</h3>
              <p>
                <b>In Short:</b> We aim to protect your personal information
                through a system of organizational and technical security
                measures.
              </p>
              <p>
                We have implemented appropriate and reasonable technical and
                organizational security measures designed to protect the
                security of any personal information we process. However,
                despite our safeguards and efforts to secure your information,
                no electronic transmission over the Internet or information
                storage technology can be guaranteed to be 100% secure, so we
                cannot promise or guarantee that hackers, cybercriminals, or
                other unauthorized third parties will not be able to defeat our
                security and improperly collect, access, steal, or modify your
                information. Although we will do our best to protect your
                personal information, the transmission of personal information
                to and from our Services is at your own risk. You should only
                access the Services within a secure environment.
              </p>
            </div>
          </Scroll.Element>
          <Scroll.Element id="collect-from-minors">
            <div
              className={classnames(styles.collect_from_minors, styles.section)}
            >
              <h3>9. DO WE COLLECT INFORMATION FROM MINORS?</h3>
              <p>
                <b>In Short:</b> We do not knowingly collect data from or market
                to children under 13 years of age.
              </p>
              <p>
                We do not knowingly solicit data from or market to children
                under 13 years of age. By using the Services, you represent that
                you are at least 13 or that you are the parent or guardian of
                such a minor and consent to such minor dependent’s use of the
                Services. If we learn that personal information from users less
                than 13 years of age has been collected, we will take reasonable
                measures to promptly delete such data from our records. If you
                become aware of any data we may have collected from children
                under the age of 13, please contact us at&nbsp;
                <a
                  className="link"
                  href="mailto:ireneneyman.illustrator@gmail.com"
                >
                  ireneneyman.illustrator@gmail.com.
                </a>
              </p>
            </div>
          </Scroll.Element>
          <Scroll.Element id="privacy-rights">
            <div className={classnames(styles.privacy_rights, styles.section)}>
              <h3>10. WHAT ARE YOUR PRIVACY RIGHTS?</h3>
              <p>
                <b>In Short:</b> In some regions, such as Australia, the
                European Economic Area (EEA), the United Kingdom (UK), and
                Canada, you have rights that allow you greater access to and
                control over your personal information. You may review, change,
                or terminate your account at any time.
              </p>
              <p>
                <b>For Australian Citizens:</b>
              </p>
              <p>
                If you are concerned that we have not complied with your legal
                rights or applicable privacy laws, you may bring a complaint
                internally through our complaints process or you may decide to
                make a formal complaint with the Office of the Australian
                Information Commissioner (
                <a className="link" href="www.oaic.gov.au">
                  www.oaic.gov.au
                </a>
                ) (which is the regulator responsible for privacy in Australia).
              </p>
              <p>
                <b>We will deal with complaints as follows:</b>
              </p>
              <p>
                <b>Step 1: let us know</b>
              </p>
              <ul>
                <li>
                  If you would like to make a complaint, you should let us know
                  by contacting our Privacy Officer (see section 14 for contact
                  details).
                </li>
              </ul>
              <p>
                <b>Step 2: investigation of complaint</b>
              </p>
              <ul>
                <li>
                  Your complaint will be investigated by our Privacy Officer.
                </li>
                <li>
                  A response to your complaint will be provided in writing
                  within a reasonable period.
                </li>
              </ul>
              <p>
                <b>Step 3: contact OAIC</b>
              </p>
              <p>
                We expect our procedures will deal fairly and promptly with your
                complaint. However, if you remain dissatisfied, you can also
                contact the Office of the Australian Information Commissioner as
                follows:
              </p>
              <p>
                Office of the Australian Information Commissioner (OAIC)
                <br />
                Complaints must be made in writing.
                <br />
                1300 363 992
                <br />
                <b>Director of Compliance</b>
                <br />
                <b>Office of the Australian</b>
                <br />
                <b>Information Commissioner</b>
                <br />
                <b>GPO Box 5218</b>
                <br />
                <b>Sydney NSW 2001</b>
                <br />
              </p>
              <p>
                <b>For the EEA, UK and Canadian Citizens</b>
              </p>
              <p>
                In some regions (like the EEA, UK, and Canada), you have certain
                rights under applicable data protection laws. These may include
                the right (i) to request access and obtain a copy of your
                personal information, (ii) to request rectification or erasure;
                (iii) to restrict the processing of your personal information;
                and (iv) if applicable, to data portability. In certain
                circumstances, you may also have the right to object to the
                processing of your personal information. You can make such a
                request by contacting us by using the contact details provided
                in the section
                <Scroll.Link
                  activeClass="active"
                  to={"how-contact-us"}
                  spy={true}
                  smooth={true}
                  offset={-100}
                  duration={1000}
                >
                  &nbsp;'
                  <a className="link">
                    HOW CAN YOU CONTACT US ABOUT THIS NOTICE?
                  </a>
                  '&nbsp;
                </Scroll.Link>
                below.
              </p>
              <p>
                We will consider and act upon any request in accordance with
                applicable data protection laws.
              </p>
              <p>
                If you are located in the EEA or UK and you believe we are
                unlawfully processing your personal information, you also have
                the right to complain to your local data protection supervisory
                authority. You can find their contact details here:{" "}
                <a
                  className="link"
                  href="https://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm"
                >
                  https://ec.europa.eu/justice/data-protection/bodies/authorities/index_en.htm.
                </a>
              </p>
              <p>
                <b>Withdrawing your consent:</b> If we are relying on your
                consent to process your personal information, which may be
                express and/or implied consent depending on the applicable law,
                you have the right to withdraw your consent at any time. You can
                withdraw your consent at any time by contacting us using the
                contact details provided in the section
                <Scroll.Link
                  activeClass="active"
                  to={"how-contact-us"}
                  spy={true}
                  smooth={true}
                  offset={-100}
                  duration={1000}
                >
                  &nbsp;'
                  <a className="link">
                    HOW CAN YOU CONTACT US ABOUT THIS NOTICE?
                  </a>
                  '&nbsp;
                </Scroll.Link>
                below.
              </p>
              <p>
                However, please note that this will not affect the lawfulness of
                the processing before its withdrawal nor, when applicable law
                allows, will it affect the processing of your personal
                information conducted in reliance on lawful processing grounds
                other than consent.
              </p>
              <p>
                <b>Cookies and similar technologies:</b> Most Web browsers are
                set to accept cookies by default. If you prefer, you can usually
                choose to set your browser to remove cookies and to reject
                cookies. If you choose to remove cookies or reject cookies, this
                could affect certain features or services of our Services.
              </p>
              <p>
                If you have questions or comments about your privacy rights, you
                may email us at{" "}
                <a
                  href="mailto:ireneneyman.illustrator@gmail.com"
                  className="link"
                >
                  ireneneyman.illustrator@gmail.com.
                </a>
              </p>
            </div>
          </Scroll.Element>
          <Scroll.Element id="do-not-track-features">
            <div
              className={classnames(
                styles.do_not_track_features,
                styles.section
              )}
            >
              <h3>11. CONTROLS FOR DO-NOT-TRACK FEATURES</h3>
              <p>
                Most web browsers and some mobile operating systems and mobile
                applications include a Do-Not-Track ('DNT') feature or setting
                you can activate to signal your privacy preference not to have
                data about your online browsing activities monitored and
                collected. At this stage, no uniform technology standard for
                recognizing and implementing DNT signals has been finalized. As
                such, we do not currently respond to DNT browser signals or any
                other mechanism that automatically communicates your choice not
                to be tracked online. If a standard for online tracking is
                adopted that we must follow in the future, we will inform you
                about that practice in a revised version of this privacy policy.
              </p>
            </div>
          </Scroll.Element>
          <Scroll.Element id="california-residents">
            <div
              className={classnames(
                styles.california_residents,
                styles.section
              )}
            >
              <h3>12. DO CALIFORNIA RESIDENTS HAVE SPECIFIC PRIVACY RIGHTS?</h3>
              <p>
                <b>In Short:</b> Yes, if you are a resident of California, you
                are granted specific rights regarding access to your personal
                information.
              </p>
              <p>
                California Civil Code Section 1798.83, also known as the 'Shine
                The Light' law, permits our users who are California residents
                to request and obtain from us, once a year and free of charge,
                information about categories of personal information (if any) we
                disclosed to third parties for direct marketing purposes and the
                names and addresses of all third parties with which we shared
                personal information in the immediately preceding calendar year.
                If you are a California resident and would like to make such a
                request, please submit your request in writing to us using the
                contact information provided below.
              </p>
              <p>
                If you are under 18 years of age, reside in California, and have
                a registered account with Services, you have the right to
                request the removal of unwanted data that you publicly post on
                the Services. To request the removal of such data, please
                contact us using the contact information provided below and
                include the email address associated with your account and a
                statement that you reside in California. We will make sure the
                data is not publicly displayed on the Services, but please be
                aware that the data may not be completely or comprehensively
                removed from all our systems (e.g. backups, etc.).
              </p>
              <p>
                <b>CCPA Privacy Notice</b>
              </p>
              <p>
                (1) every individual who is in the State of California for other
                than a temporary or transitory purpose and
                <br />
                (2) every individual who is domiciled in the State of California
                who is outside the State of California for a temporary or
                transitory purpose
              </p>
              <p>All other individuals are defined as 'non-residents'.</p>
              <p>
                If this definition of 'resident' applies to you, we must adhere
                to certain rights and obligations regarding your personal
                information.
              </p>
              <p>
                <b>What categories of personal information do we collect?</b>
              </p>
              <p>
                We have collected the following categories of personal
                information in the past twelve (12) months:
              </p>
              <table>
                <thead>
                  <tr>
                    <th>Category</th>
                    <th>Examples</th>
                    <th>Collected</th>
                  </tr>
                </thead>
                <tr>
                  <td>A. Identifiers</td>
                  <td>
                    Contact details, such as real name, alias, postal address,
                    telephone or mobile contact number, unique personal
                    identifier, online identifier, Internet Protocol address,
                    email address, and account name
                  </td>
                  <td>NO</td>
                </tr>
                <tr>
                  <td>
                    B. Personal information categories listed in the California
                    Customer Records statute
                  </td>
                  <td>
                    Name, contact information, education, employment, employment
                    history, and financial information
                  </td>
                  <td>NO</td>
                </tr>
                <tr>
                  <td>
                    C. Protected classification characteristics under California
                    or federal law
                  </td>
                  <td>Gender and date of birth</td>
                  <td>NO</td>
                </tr>
                <tr>
                  <td>D. Commercial information</td>
                  <td>
                    Transaction information, purchase history, financial
                    details, and payment information
                  </td>
                  <td>NO</td>
                </tr>
                <tr>
                  <td>E. Biometric information</td>
                  <td>Fingerprints and voiceprints</td>
                  <td>NO</td>
                </tr>
                <tr>
                  <td>F. Internet or other similar network activity</td>
                  <td>
                    Browsing history, search history, online behaviour, interest
                    data, and interactions with our and other websites,
                    applications, systems, and advertisements
                  </td>
                  <td>NO</td>
                </tr>
                <tr>
                  <td>G. Geolocation data</td>
                  <td>Device location</td>
                  <td>NO</td>
                </tr>
                <tr>
                  <td>
                    H. Audio, electronic, visual, thermal, olfactory, or similar
                    information
                  </td>
                  <td>
                    Images and audio, video or call recordings created in
                    connection with our business activities
                  </td>
                  <td>NO</td>
                </tr>
                <tr>
                  <td>I. Professional or employment-related information</td>
                  <td>
                    Business contact details in order to provide you our
                    Services at a business level or job title, work history, and
                    professional qualifications if you apply for a job with us
                  </td>
                  <td>NO</td>
                </tr>
                <tr>
                  <td>J. Education Information</td>
                  <td>Student records and directory information</td>
                  <td>NO</td>
                </tr>
                <tr>
                  <td>K. Inferences drawn from other personal information</td>
                  <td>
                    Inferences drawn from any of the collected personal
                    information listed above to create a profile or summary
                    about, for example, an individual’s preferences and
                    characteristics
                  </td>
                  <td>NO</td>
                </tr>
              </table>
              <p>
                We may also collect other personal information outside of these
                categories through instances where you interact with us in
                person, online, or by phone or mail in the context of:
              </p>
              <ul>
                <li>Receiving help through our customer support channels;</li>
                <li>Participation in customer surveys or contests; and</li>
                <li>
                  Facilitation the delivery of our Services and responding to
                  your inquiries.
                </li>
              </ul>
              <p>
                <b>How do we use and share your personal information?</b>
              </p>
              <p>
                More information about our data collection and sharing practices
                can be found in this privacy policy.
              </p>
              <p>
                You may contact us by email at
                <a
                  className="link"
                  href="mailto:ireneneyman.illustrator@gmail.com"
                >
                  ireneneyman.illustrator@gmail.com
                </a>
                , or by referring to the contact details at the bottom of this
                document.
              </p>
              <p>
                If you are using an authorized agent to exercise your right to
                opt out we may deny a request if the authorized agent does not
                submit proof that they have been validly authorized to act on
                your behalf.
              </p>
              <p>
                <b>Will your information be shared with anyone else?</b>
              </p>
              <p>
                We may disclose your personal information with our service
                providers pursuant to a written contract between us and each
                service provider. Each service provider is a for-profit entity
                that processes the information on our behalf, following the same
                strict privacy protection obligations mandated by the CCPA.
              </p>
              <p>
                We may use your personal information for our own business
                purposes, such as for undertaking internal research for
                technological development and demonstration. This is not
                considered to be 'selling' of your personal information.
              </p>
              <p>
                Iryna Yancheva has not sold or shared any personal information
                to third parties for a business or commercial purpose in the
                preceding twelve (12) months.
              </p>
              <p>
                The categories of third parties to whom we disclosed personal
                information for a business or commercial purpose can be found
                under '
                <Scroll.Link
                  activeClass="active"
                  to={"with-whom-share"}
                  spy={true}
                  smooth={true}
                  offset={-100}
                  duration={1000}
                >
                  &nbsp;
                  <a className="link">
                    WHEN AND WITH WHOM DO WE SHARE YOUR PERSONAL INFORMATION?
                  </a>
                  &nbsp;
                </Scroll.Link>
                '.
              </p>
              <p>
                <b>Your rights with respect to your personal data</b>
              </p>
              <p>
                <b>Right to request deletion of the data — Request to delete</b>
              </p>
              <p>
                You can ask for the deletion of your personal information. If
                you ask us to delete your personal information, we will respect
                your request and delete your personal information, subject to
                certain exceptions provided by law, such as (but not limited to)
                the exercise by another consumer of his or her right to free
                speech, our compliance requirements resulting from a legal
                obligation, or any processing that may be required to protect
                against illegal activities.
              </p>
              <p>
                <b>Right to be informed — Request to know</b>
              </p>
              <p>Depending on the circumstances, you have a right to know:</p>
              <ul>
                <li>whether we collect and use your personal information;</li>
                <li>the categories of personal information that we collect;</li>
                <li>
                  the purposes for which the collected personal information is
                  used;
                </li>
                <li>
                  whether we sell or share personal information to third
                  parties;
                </li>
                <li>
                  the categories of personal information that we sold, shared,
                  or disclosed for a business purpose;
                </li>
                <li>
                  the categories of third parties to whom the personal
                  information was sold, shared, or disclosed for a business
                  purpose;
                </li>
                <li>
                  the business or commercial purpose for collecting, selling, or
                  sharing personal information; and
                </li>
                <li>
                  the specific pieces of personal information we collected about
                  you.
                </li>
              </ul>
              <p>
                In accordance with applicable law, we are not obligated to
                provide or delete consumer information that is de-identified in
                response to a consumer request or to re-identify individual data
                to verify a consumer request.
              </p>
              <p>
                <b>
                  Right to Non-Discrimination for the Exercise of a Consumer’s
                  Privacy Rights
                </b>
              </p>
              <p>
                We will not discriminate against you if you exercise your
                privacy rights.
              </p>
              <p>
                <b>
                  Right to Limit Use and Disclosure of Sensitive Personal
                  Information
                </b>
              </p>
              <p>
                We do not process consumer's sensitive personal information.
              </p>
              <p>
                <b>Verification process</b>
              </p>
              <p>
                Upon receiving your request, we will need to verify your
                identity to determine you are the same person about whom we have
                the information in our system. These verification efforts
                require us to ask you to provide information so that we can
                match it with information you have previously provided us. For
                instance, depending on the type of request you submit, we may
                ask you to provide certain information so that we can match the
                information you provide with the information we already have on
                file, or we may contact you through a communication method (e.g.
                phone or email) that you have previously provided to us. We may
                also use other verification methods as the circumstances
                dictate.
              </p>
              <p>
                We will only use personal information provided in your request
                to verify your identity or authority to make the request. To the
                extent possible, we will avoid requesting additional information
                from you for the purposes of verification. However, if we cannot
                verify your identity from the information already maintained by
                us, we may request that you provide additional information for
                the purposes of verifying your identity and for security or
                fraud-prevention purposes. We will delete such additionally
                provided information as soon as we finish verifying you.
              </p>
              <p>
                <b>Other privacy rights</b>
              </p>
              <ul>
                <li>
                  You may object to the processing of your personal information.
                </li>
                <li>
                  You may request correction of your personal data if it is
                  incorrect or no longer relevant, or ask to restrict the
                  processing of the information.
                </li>
                <li>
                  You can designate an authorized agent to make a request under
                  the CCPA on your behalf. We may deny a request from an
                  authorized agent that does not submit proof that they have
                  been validly authorized to act on your behalf in accordance
                  with the CCPA.
                </li>
                <li>
                  You may request to opt out from future selling or sharing of
                  your personal information to third parties. Upon receiving an
                  opt-out request, we will act upon the request as soon as
                  feasibly possible, but no later than fifteen (15) days from
                  the date of the request submission.
                </li>
              </ul>
              <p>
                To exercise these rights, you can contact us by email at
                <a
                  className="link"
                  href="mailto:ireneneyman.illustrator@gmail.com"
                >
                  &nbsp;ireneneyman.illustrator@gmail.com&nbsp;
                </a>
                , or by referring to the contact details at the bottom of this
                document. If you have a complaint about how we handle your data,
                we would like to hear from you.
              </p>
            </div>
          </Scroll.Element>
          <Scroll.Element id="update-to-notice">
            <div
              className={classnames(styles.update_to_notice, styles.section)}
            >
              <h3>13. DO WE MAKE UPDATES TO THIS NOTICE?</h3>
              <p>
                <b>In Short:</b> Yes, we will update this notice as necessary to
                stay compliant with relevant laws.
              </p>
              <p>
                We may update this privacy policy from time to time. The updated
                version will be indicated by an updated 'Revised' date and the
                updated version will be effective as soon as it is accessible.
                If we make material changes to this privacy policy, we may
                notify you either by prominently posting a notice of such
                changes or by directly sending you a notification. We encourage
                you to review this privacy policy frequently to be informed of
                how we are protecting your information.
              </p>
            </div>
          </Scroll.Element>
          <Scroll.Element id="how-contact-us">
            <div className={classnames(styles.how_contact_us, styles.section)}>
              <h3>14. HOW CAN YOU CONTACT US ABOUT THIS NOTICE?</h3>
              <p>
                If you have questions or comments about this notice, you may
                email us at{" "}
                <a
                  className="link"
                  href="mailto:ireneneyman.illustrator@gmail.com"
                >
                  ireneneyman.illustrator@gmail.com
                </a>
                .
              </p>
            </div>
          </Scroll.Element>
          <Scroll.Element id="review-or-delete">
            <div
              className={classnames(styles.review_or_delete, styles.section)}
            >
              <h3>
                15. HOW CAN YOU REVIEW, UPDATE, OR DELETE THE DATA WE COLLECT
                FROM YOU?
              </h3>
              <p>
                <b>In Short:</b> Yes, we will update this notice as necessary to
                stay compliant with relevant laws.
              </p>
              <p>
                Based on the applicable laws of your country, you may have the
                right to request access to the personal information we collect
                from you, change that information, or delete it. To request to
                review, update, or delete your personal information, please
                email us at:{" "}
                <a
                  className="link"
                  href="mailto:ireneneyman.illustrator@gmail.com"
                >
                  ireneneyman.illustrator@gmail.com.
                </a>
              </p>
            </div>
          </Scroll.Element>
        </div>
      </Layout>
    </>
  );
};
