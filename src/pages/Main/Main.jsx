import Layout from "../../components/Layout";
import Masonry from "../../components/Masonry";
import styles from "./Main.module.scss";
import homeImage from "../../assets/images/home_image.webp";
import homeStar from "../../assets/images/star_home.svg";
import * as Scroll from "react-scroll";
import { Helmet } from "react-helmet";

export const Main = () => {
  return (
    <Layout>
      <Helmet>
        <title>Irene Neyman</title>
      </Helmet>
      <section>
        <h1 className={styles.hello}>Hi! I’m Irene</h1>
        <h3 className={styles.about}>
          Curious and dynamic illustrator & graphic designer, who creates a
          strong visual and emotional connection between brand, design, and
          audience.
        </h3>
        <img className={styles.home_image} src={homeImage} alt="home" />
        <Scroll.Link
          activeClass="active"
          to="masonry"
          spy={true}
          smooth={true}
          offset={-100}
          duration={1000}
        >
          <div className={styles.scroller}>
            <img src={homeStar} alt="" />
            <div className={styles.text}>View projects</div>
          </div>
        </Scroll.Link>
      </section>
      <Scroll.Element id="masonry">
        <Masonry />
      </Scroll.Element>
    </Layout>
  );
};
