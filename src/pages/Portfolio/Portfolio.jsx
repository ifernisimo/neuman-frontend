import Layout from "../../components/Layout";
import Masonry from "../../components/Masonry";
import { Helmet } from "react-helmet";

export const Portfolio = () => {
  return (
    <Layout>
      <Helmet>
        <title>Irene Neyman / Works</title>
      </Helmet>
      <Masonry />
    </Layout>
  );
};
