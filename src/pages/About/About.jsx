import Layout from "../../components/Layout";
import styles from "./About.module.scss";
import photo from "../../assets/images/photo.webp";
import { Link, useNavigate } from "react-router-dom";
import { Helmet } from "react-helmet";
import homeStar from "../../assets/images/star_home.svg";
import classNames from "classnames";

export const About = () => {
  const navigate = useNavigate();
  return (
    <>
      <Helmet>
        <title>Irene Neyman / About</title>
      </Helmet>
      <Layout>
        <div className={styles.about_wrapper}>
          <div className={styles.title}>
            <span>Hi there,</span>
            <span>I’m Irene</span>
          </div>
          <div className={styles.about}>
            <div className={styles.leftContent}>
              <div className={classNames(styles.photo, styles.hidden)}>
                <img className={styles.personal} src={photo} alt="My photo" />
              </div>
              <div className={styles.description}>
                <div className={styles.text}>
                  <p>
                    I’m a professional illustrator & graphic designer, who
                    creates a strong visual and emotional (and also a little
                    magical) connection between brand, design, and audience.
                    Originally from Ukraine, but now based in Canada and working
                    with projects worldwide.
                  </p>
                  <p>
                    I decided to connect my life with a career in art because I
                    have been immersed in illustrating since I was a kid. My
                    path in illustration & design began more than 4 years ago
                    when I decided to completely change the course of my life
                    journey.
                  </p>
                  <p>
                    Specializing in, but not limited to, illustrations & designs
                    for editorial, apps, websites, brands, publications,
                    character design and so much more.
                  </p>
                  <p>
                    I am dedicated to creating designs that are not only
                    aesthetically pleasing, but also effective in communicating
                    the message of my clients. I have worked with a variety of
                    clients from different industries, and I am always excited
                    to take on new challenges and push my creative boundaries.
                  </p>
                </div>{" "}
                <div className={styles.features}>
                  <h3>Features & Recognition</h3>
                  <div className={styles.info}>
                    <span>Citycelebrity, 2021</span>
                    <br />
                    <span>Meta History, 2022</span>
                    <br />
                    <span>
                      Festival Onomatepeia
                      <span className={styles.where}>
                        &nbsp;(Valongo, Portugal)
                      </span>
                      , 05/2022
                    </span>
                    <br />
                    <span>
                      Exhibition ACUA
                      <span className={styles.where}>
                        &nbsp; (Edmonton, Canada)
                      </span>
                      , 11/2022
                    </span>
                    <br />
                    <span>
                      <a
                        href="https://weandthecolor.com/irene-neyman-illustrations/172422"
                        className="link"
                        alt="Design rush"
                      >
                        We And The Color
                      </a>
                      , 01/2023
                    </span>
                    <br />
                    <span>
                      <a
                        href="https://shoutoutla.com/meet-irene-neyman-illustrator-graphic-designer/"
                        className="link"
                        alt="Shoutout LA"
                      >
                        Shoutout LA
                      </a>
                      , 02/2023
                    </span>
                    <br />
                    <span>
                      <a
                        href="https://www.designrush.com/best-designs/packaging/trends/best-bottle-label-designs"
                        className="link"
                        alt="Design rush"
                      >
                        Best Design Awards by DesignRush
                      </a>
                      <span className={styles.where}>
                        &nbsp;(The Best Bottle Label Designs for&nbsp;
                        <a
                          href="https://ireneneyman.com/case/44"
                          className="link"
                          alt="Space milk case"
                        >
                          Space Milk Project
                        </a>
                        )
                      </span>
                      , 02/2023
                    </span>
                    <br />
                    <span>
                      <a
                        href="https://illustratorslounge.com/interviews/an-interview-with-irene-neyman/"
                        className="link"
                        alt="Illustrators Lounge"
                      >
                        Illustrators' Lounge
                      </a>
                      , 03/2023
                    </span>
                    <br />
                    <span>
                      Applied Arts 2023, Winner | Illustration Awards, 03/2023
                    </span>
                    <br />
                    <span>
                      World Illustration Awards 2023, Longlist | Exploration,
                      05/2023
                    </span>
                  </div>
                </div>
                <div className={styles.inquiries}>
                  <h3>Have work inquiries?</h3>
                  <h3>Just send me a message and let’s talk!</h3>
                  <div
                    className={styles.getInTouch}
                    onClick={() => navigate("/contacts")}
                  >
                    <img className={styles.rotate} src={homeStar} alt="" />
                    <div className={styles.text}>Get in touch</div>
                  </div>
                </div>
              </div>
            </div>
            <div className={styles.photo}>
              <img className={styles.personal} src={photo} alt="My photo" />
            </div>
          </div>
        </div>
      </Layout>
    </>
  );
};
