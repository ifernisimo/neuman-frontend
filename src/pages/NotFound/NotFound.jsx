import Layout from "../../components/Layout";
import styles from "./NotFound.module.scss";
import { Link } from "react-router-dom";
import notFound from "../../assets/images/404.webp";
import { Helmet } from "react-helmet";

export const NotFound = () => {
  return (
    <>
      <Helmet>
        <title>Irene Neyman / 404 Not found</title>
      </Helmet>
      <Layout>
        <div className={styles.notFound}>
          <h1 className={styles.title}>Oops! Page not found</h1>
          <div className={styles.description}>
            We couldn't find the page you were looking for.
            <br />
            You can return to the&nbsp;
            <Link to="/" alt="Home page">
              homepage
            </Link>
            .
          </div>
          <img src={notFound} alt="404 not found" />
        </div>
      </Layout>
    </>
  );
};
