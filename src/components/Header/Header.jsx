import styles from "./Header.module.scss";
import Nav from "../Nav";
import { Link } from "react-router-dom";
import logoCenter from "../../assets/images/logo-center.svg";
import { useEffect } from "react";

export const Header = () => {
  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <>
      <div className={styles.header}>
        <Link to="/" alt="Home page">
          <div className={styles.logo}>
            <img className={styles.in} src={logoCenter} alt="" />
          </div>
        </Link>
        <Nav />
      </div>
    </>
  );
};
