import styles from "./Sidebar.module.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInstagram } from "@fortawesome/free-brands-svg-icons";
import { faDribbble } from "@fortawesome/free-brands-svg-icons";
import { faBehance } from "@fortawesome/free-brands-svg-icons";
import { faLinkedinIn } from "@fortawesome/free-brands-svg-icons";

export const Sidebar = () => {
  return (
    <>
      <div className={styles.sidebar}>
        <a
          href="https://www.instagram.com/irene_neyman/"
          target="_blank"
          alt="Instagram"
        >
          <FontAwesomeIcon icon={faInstagram} />
        </a>
        <a
          href="https://dribbble.com/Irene_Neyman"
          target="_blank"
          alt="Dribbble"
        >
          <FontAwesomeIcon icon={faDribbble} />
        </a>
        <a
          href="https://www.behance.net/irene_neyman"
          target="_blank"
          alt="Behance"
        >
          <FontAwesomeIcon icon={faBehance} />
        </a>
        <a
          href="https://www.linkedin.com/in/ireneneyman/"
          target="_blank"
          alt="Linkedin"
        >
          <FontAwesomeIcon icon={faLinkedinIn} />
        </a>
      </div>
    </>
  );
};
