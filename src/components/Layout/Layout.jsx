import Footer from "../Footer";
import Header from "../Header";
import ScrollToTop from "../ScrollToTop";
import Sidebar from "../Sidebar";

export const Layout = ({ children }) => {
  return (
    <>
      <div className={"container"}>
        <Header />
        <Sidebar />
        <div>{children}</div>
        <Footer />
        <ScrollToTop />
      </div>
    </>
  );
};
