import styles from "./Footer.module.scss";
import divider from "../../assets/images/divider.svg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInstagram } from "@fortawesome/free-brands-svg-icons";
import { faDribbble } from "@fortawesome/free-brands-svg-icons";
import { faBehance } from "@fortawesome/free-brands-svg-icons";
import { faLinkedinIn } from "@fortawesome/free-brands-svg-icons";
import { Link } from "react-router-dom";

export const Footer = () => {
  return (
    <>
      <div className={styles.footer}>
        <img className={styles.divider} src={divider} alt="divider" />
        <div className={styles.social}>
          <a
            href="https://www.instagram.com/irene_neyman/"
            target="_blank"
            alt="Instagram"
          >
            <FontAwesomeIcon icon={faInstagram} />
          </a>
          <a
            href="https://dribbble.com/Irene_Neyman"
            target="_blank"
            alt="Dribbble"
          >
            <FontAwesomeIcon icon={faDribbble} />
          </a>
          <a
            href="https://www.behance.net/irene_neyman"
            target="_blank"
            alt="Behance"
          >
            <FontAwesomeIcon icon={faBehance} />
          </a>
          <a
            href="https://www.linkedin.com/in/ireneneyman/"
            target="_blank"
            alt="Linkedin"
          >
            <FontAwesomeIcon icon={faLinkedinIn} />
          </a>
        </div>
        <a
          href={
            "mailto:ireneneyman.illustrator@gmail.com?subject=Website_email"
          }
          className={styles.email}
        >
          ireneneyman.illustrator@gmail.com
        </a>
        <div className={styles.terms}>
          <span className={styles.conditions}>
            <Link to="/terms-of-use" alt="Terms ">
              Terms of Use •&nbsp;
            </Link>
            <Link to="/privacy-policy" alt="Privacy Policy">
              Privacy Policy
            </Link>
          </span>
          <span>© 2023 Irene Neyman. All rights reserved.</span>
        </div>
      </div>
    </>
  );
};
