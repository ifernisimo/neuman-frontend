import { gql } from "@apollo/client";

export const GetCases = gql`
  query GetCases($position: Int) {
    cases(pagination: { start: $position, limit: 10 }, sort: "id:DESC") {
      data {
        id
        attributes {
          title
          description
          cover {
            data {
              attributes {
                url
              }
            }
          }
          section {
            sub_description
            images {
              data {
                attributes {
                  url
                }
              }
            }
          }
        }
      }
    }
  }
`;
