import MasonryWall from "react-masonry-css";
import styles from "./Masonry.module.scss";
import { useQuery } from "@apollo/client";
import { GetCases } from "./service/query";
import { setAbsolutePath } from "../../utils/urlUtils";
import { Link } from "react-router-dom";
import sal from "sal.js";
import { useEffect, useState } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import star from "../../assets/images/star.svg";
import ReactGA from "react-ga4";

export const Masonry = () => {
  const { loading, error, data, fetchMore } = useQuery(GetCases, {
    variables: { position: 0 },
  });

  const [cases, setCases] = useState([]);
  const [position, setPosition] = useState(0);
  const [end, setEnd] = useState(false);
  const [fetched, setFetched] = useState(false);

  const fetchNext = () => {
    setFetched(true);
    setPosition(position + 10);
    fetchMore({
      variables: { position },
      updateQuery: (prevResult, { fetchMoreResult }) => {
        if (!fetchMoreResult.cases.data.length) {
          setEnd(true);
        }
        return setCases([...cases, ...fetchMoreResult.cases.data]);
      },
    });
  };

  setTimeout(() => {
    if (!cases.length && !fetched) {
      fetchNext();
    }
  }, 2000);

  useEffect(() => fetchNext(), []);

  const GaLogEvent = ({ label }) => {
    ReactGA.event({
      category: "Case view",
      action: "case_view",
      label,
      nonInteraction: true,
      transport: "xhr",
    });
  };

  const massonryBreakpointsConfig = {
    default: 3,
    1024: 2,
    576: 1,
  };

  useEffect(() => {
    if (!loading)
      sal({
        options: {
          once: false,
          threshold: 0.1,
        },
      });
  });

  return (
    <InfiniteScroll
      dataLength={cases.length}
      next={fetchNext}
      hasMore={true}
      loader={end ? <></> : <h4>Loading...</h4>}
    >
      <MasonryWall
        breakpointCols={massonryBreakpointsConfig}
        className={styles["my-masonry-grid"]}
        columnClassName={styles["my-masonry-grid_column"]}
      >
        {!loading && cases.length ? (
          cases.map((item, index) => {
            return (
              <div
                data-sal="fade"
                style={{ "--sal-duration": "1.5s", "--sal-delay": "0.3s" }}
                className={styles.cover_wrapper}
                key={index}
              >
                <Link
                  to={`/case/${item.id}`}
                  onClick={() => GaLogEvent(item.attributes.title)}
                  alt={item.attributes.title}
                >
                  <div className={styles.cover}>
                    <span>{item.attributes.title}</span>
                    <img className={styles.star_icon} src={star} alt="" />
                  </div>
                </Link>
                <img
                  src={setAbsolutePath(
                    item.attributes.cover.data.attributes.url
                  )}
                  alt={item.attributes.title}
                />
              </div>
            );
          })
        ) : (
          <></>
        )}
      </MasonryWall>
    </InfiniteScroll>
  );
};
