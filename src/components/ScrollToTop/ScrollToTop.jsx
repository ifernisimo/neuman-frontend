import { useState } from "react";
import * as Scroll from "react-scroll";
import arrow from "../../assets/images/to_top_arrow.svg";
import styles from "./ScrollToTop.module.scss";
import classnames from "classnames";

export const ScrollToTop = () => {
  const [visible, setVisible] = useState(false);
  const toTop = () => {
    Scroll.animateScroll.scrollToTop();
  };

  const showHeight = () => {
    window.addEventListener("scroll", () => {
      if (window.scrollY > 1000) {
        setVisible(true);
      } else {
        setVisible(false);
      }
    });
  };
  showHeight();

  return (
    <img
      className={classnames(
        styles.arrowTop,
        visible ? styles.visible : styles.hidden
      )}
      onClick={() => toTop()}
      src={arrow}
      alt="Scroll to top"
    />
  );
};
