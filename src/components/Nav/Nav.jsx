import styles from "./Nav.module.scss";
import { NavLink } from "react-router-dom";

export const Nav = () => {
  return (
    <>
      <nav className={styles.nav}>
        <div className={styles.nav_item}>
          <NavLink
            className={({ isActive }) => (isActive ? styles.activeBtn : "")}
            to="/works"
          >
            WORKS
          </NavLink>
        </div>
        <div className={styles.nav_item}>
          <NavLink
            className={({ isActive }) => (isActive ? styles.activeBtn : "")}
            to="/about"
          >
            ABOUT
          </NavLink>
        </div>
        <div className={styles.nav_item}>
          <NavLink
            className={({ isActive }) => (isActive ? styles.activeBtn : "")}
            to="/contacts"
          >
            CONTACTS
          </NavLink>
        </div>
      </nav>
    </>
  );
};
