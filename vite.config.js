import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import babel from "vite-plugin-babel";
import macrosPlugin from "vite-plugin-babel-macros";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    react(),
    babel({
      babelConfig: {
        plugins: ["macros"],
      },
    }),
    macrosPlugin({
      "fontawesome-svg-core": {
        license: "free",
      },
    }),
  ],
});
