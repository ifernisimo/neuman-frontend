#!/bin/bash

rsync -zavP --exclude-from=./rsyncignore.txt ./* sysadmin@179.61.219.253:/var/www/ireneneyman.com/public
rsync -zavP ./.env.production sysadmin@179.61.219.253:/var/www/ireneneyman.com/public
